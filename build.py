#!/usr/bin/env python

import argparse
import nose
import os
import shutil
import subprocess
import sys


cmds = ['run_tests', 'local_install', 'clean']
build_artifacts = ['build', 'dist', 'mp3organiser.egg-info']


def run_tests():
    success = nose.run(argv=[''])
    print 'Unit Tests %s' % (success and 'Passed' or 'Failed')

    success = 0
    for parent, dirs, files in os.walk('mp3organiser'):
        for f in [f for f in files if f.endswith('.py')]:
            success += subprocess.call(['pep8', '-r', '--count',
                                       os.path.join(parent, f)])

    print 'PEP8 %s' % (success == 0 and
          'Passed' or 'Failed with %s errors' % success)


def local_install():
#    subprocess.call(['virtualenv', 'venv', '--no-site-packages'])
    os.system('virtualenv --no-site-packages venv; source venv/bin/activate; \
               ./setup.py install')


def clean():
    for artifact in build_artifacts:
        try:
            print 'Removing %s... ' % artifact,
            shutil.rmtree(artifact)
            print ' Done'
        except OSError as e:
            print 'An error occurred: %s' % e

    print 'Removing pyc files...',
    for parent, _, filename in os.walk('.'):
        for f in [f for f in filename if f.endswith('pyc')]:
            os.remove(os.path.join(parent, f))
    print ' Done'


def get_args(sysargs):
    parser = argparse.ArgumentParser(description=("""
    Run tests, install in the local environment or clean up
    """))
    parser.add_argument('cmd', help=("""
                The command to run. Either:
                %s
                """ % ' OR '.join(cmds)))
    return parser.parse_args(sysargs)

if __name__ == '__main__':
    args = get_args(sys.argv[1:])
    locals()[args.cmd]()
